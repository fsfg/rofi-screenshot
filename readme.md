*forked from [rofi-screenshot](https://github.com/ceuk/rofi-screenshot)*


## rofi-screenshot

![](https://imgur.com/7io5BKJ.gif)

A script to show and execute various screen capture related commands in Rofi.

### Features
* Capture a region to the clipboard or a directory
* Capture the whole screen to the clipboard or a directory
* Record a specific region and save as a gif or MP4
* Record the whole screen and save as a gif or MP4

### Installation
Make sure you have the required [dependencies](#dependencies) installed.

Clone the repo, make rofi-screenshot executable
```bash
$ git clone --depth 1 https://gitlab.com/fsfg/rofi-screenshot.git
$ cd rofi-screenshot && chmod u+x rofi-screenshot
```
Then move it to somewhere in your $PATH, i.e.:
```bash
$ cp rofi-screenshot ~/.local/bin/
```

### Usage
Show the rofi menu
```bash
$ rofi-screenshot
```

Stop recording
```bash
$ rofi-screenshot -s
```
**Tip**: Add a keybinding for both of the above commands

### Dependencies

* [rofi](https://github.com/davatorium/rofi)
* [ffcast](https://github.com/lolilolicon/FFcast)
* [slop](https://github.com/naelstrof/slop)
* [xclip](https://github.com/astrand/xclip)
